"""studentenrollment URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from .views import AddUser, getAllUser, editUsers, updateUsers, AddCourses, getAllCourses, editUsers, updateCourse, \
AddTerm, getAllTerm, editTerm, updateTerm, addRoom, getAllRoom, editRoom, updateRoom, addDate, getAllDate, editDate, \
updateDate, AddStudent, getAllStudent, editStudent, UpdateStudent, deleteUsers, deleteCourse, deleteTerm, deleteRoom, deleteDate,\
    deleteStudent

urlpatterns = [
    path('AddUser/', AddUser),
    path('getAllUser/', getAllUser),
    path('editUsers/', editUsers),
    path('updateUsers/', updateUsers),
    path('AddCourses/', AddCourses),
    path('getAllCourses/', getAllCourses),
    path('updateCourse/', updateCourse),
    path('AddTerm/', AddTerm),
    path('getAllTerm/', getAllTerm),
    path('editTerm/', editTerm),
    path('updateTerm/', updateTerm),
    path('addRoom/', addRoom),
    path('getAllRoom/', getAllRoom),
    path('editRoom/', editRoom),
    path('updateRoom/', updateRoom),
    path('getAllDate/', getAllDate),
    path('addDate/', addDate),
    path('editDate/', editDate),
    path('updateDate/', updateDate),
    path('AddStudent/', AddStudent),
    path('getAllStudent/', getAllStudent),
    path('editStudent/', editStudent),
    path('UpdateStudent/', UpdateStudent),
    path('deleteUsers/', deleteUsers),
    path('deleteCourse/', deleteCourse),
    path('deleteTerm/', deleteTerm),
    path('deleteRoom/', deleteRoom),
    path('deleteDate/', deleteDate),
    path('deleteStudent/', deleteStudent),

]
