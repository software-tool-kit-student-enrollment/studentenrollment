from django.http import HttpResponse
from rest_framework.response import Response
from rest_framework.decorators import api_view
from .models import User, Courses, Term, Room, Students, DateOfCourse
from datetime import datetime


def AddUser(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        userobj = User()
        userobj.username(username)
        userobj.password(password)
        userobj.save()
    return Response({"status": "success"})

def getAllUser(request):
    userobj = User()
    userreturn = userobj.objects.all()
    return Response({"returnval": userreturn})

def editUsers(request):
    userid = request.GET['userid']
    userobj = User()
    userreturn = userobj.objects.get(userid = userid)
    return Response({"returnval": userreturn})

def updateUsers(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        userid = request.POST['userid']
        userobj = User()
        userobj.username(username)
        userobj.password(password)
        userobj.userid(userid)
        userobj.save()
    return Response({"status": "success"})

def deleteUsers(request):
    userid = request.GET['userid']
    object = User()
    object.objects.filter(userid = userid).delete()
    return Response({"status": "success"})

def AddCourses(request):
    if request.method == "POST":
        courseName = request.POST['courseName']
        object = Courses()
        object.courseName(courseName)
        object.save()
    return Response({"status": "success"})

def getAllCourses(request):
    object = Courses()
    objectreturn = object.objects.all()
    return Response({"returnval": objectreturn})

def editUsers(request):
    courseid = request.GET['courseid']
    object = Courses()
    objectreturn = object.objects.get(courseid = courseid)
    return Response({"returnval": objectreturn})

def updateCourse(request):
    if request.method == "POST":
        courseid = request.POST['courseid']
        courseName = request.POST['courseName']
        object = Courses()
        object.courseName(courseName)
        object.courseid(courseid)
        object.save()
    return Response({"status": "success"})

def deleteCourse(request):
    courseid = request.GET['courseid']
    object = Courses()
    object.objects.filter(courseid = courseid).delete()
    return Response({"status": "success"})

def AddTerm(request):
    if request.method == "POST":
        termName = request.POST['termName']
        termCourseId = request.POST['courseid']
        object = Term()
        object.termcourseid(termCourseId)
        object.termname(termName)
        object.save()
    return Response({"status": "success"})

def getAllTerm():
    object = Term()
    objectreturn = object.objects.all()
    return Response({"returnval": objectreturn})

def editTerm(request):
    termid = request.GET['termid']
    object = Term()
    objectreturn = object.objects.get(termid = termid)
    return Response({"returnval": objectreturn})

def updateTerm(request):
    if request.method == "POST":
        termName = request.POST['termName']
        termCourseId = request.POST['courseid']
        termid = request.POST['termid']
        object = Term()
        object.termid(termid)
        object.termcourseid(termCourseId)
        object.termname(termName)
        object.save()
    return Response({"status": "success"})

def deleteTerm(request):
    termid = request.GET['termid']
    object = Term()
    object.objects.filter(termid = termid).delete()
    return Response({"status": "success"})

def addRoom(request):
    if request.method == "POST":
        roomName = request.POST['roomName']
        roomCourseId = request.POST['courseid']
        object = Room()
        object.roomCourseId(roomCourseId)
        object.roomname(roomName)
        object.save()
    return Response({"status": "success"})

def getAllRoom():
    object = Room()
    objectreturn = object.objects.all()
    return Response({"returnval": objectreturn})

def editRoom(request):
    roomid = request.GET['roomid']
    object = Room()
    objectreturn = object.objects.get(roomid = roomid)
    return Response({"returnval": objectreturn})

def updateRoom(request):
    if request.method == "POST":
        roomName = request.POST['roomName']
        roomCourseId = request.POST['courseid']
        roomid = request.POST['roomid']
        object = Room()
        object.roomid(roomid)
        object.roomCourseId(roomCourseId)
        object.roomname(roomName)
        object.save()
    return Response({"status": "success"})

def deleteRoom(request):
    roomid = request.GET['roomid']
    object = Room()
    object.objects.filter(roomid = roomid).delete()
    return Response({"status": "success"})

def addDate(request):
    if request.method == "POST":
        dateconducted = request.POST['dateconducted']
        datetimed = request.POST['datetimed']
        datecourseid = request.POST['datecourseid']
        dateterm = request.POST['dateterm']
        object = DateOfCourse()
        object.dateConducted(dateconducted)
        object.dateCourseid(datecourseid)
        object.dateTerm(dateterm)
        object.dateTime(datetimed)
        object.save()
    return Response({"status": "success"})

def getAllDate():
    object = DateOfCourse()
    objectreturn = object.objects.all()
    return Response({"returnval": objectreturn})

def editDate(request):
    dateid = request.GET['dateid']
    object = DateOfCourse()
    objectreturn = object.objects.get(dateid = dateid)
    return Response({"returnval": objectreturn})

def updateDate(request):
    if request.method == "POST":
        dateconducted = request.POST['dateconducted']
        datetimed = request.POST['datetimed']
        datecourseid = request.POST['datecourseid']
        dateterm = request.POST['dateterm']
        dateid = request.POST['dateid']
        object = DateOfCourse()
        object.dateConducted(dateconducted)
        object.dateCourseid(datecourseid)
        object.dateTerm(dateterm)
        object.dateTime(datetimed)
        object.dateId(dateid)
        object.save()
    return Response({"status": "success"})

def deleteDate(request):
    dateid = request.GET['dateid']
    object = DateOfCourse()
    object.objects.filter(dateid = dateid).delete()
    return Response({"status": "success"})

def AddStudent(request):
    if request.method == "POST":
        studentName = request.POST['studentName']
        studentCourse = request.POST['studentCourse']
        studentTermId = request.POST['studentTermId']
        studentRoomId = request.POST['studentRoomId']
        studentDateOfCourse = request.POST['studentDateOfCourse']
        object = Students()
        object.studentname(studentName)
        object.studentcourse(studentCourse)
        object.studenttermid(studentTermId)
        object.studentRoomid(studentRoomId)
        object.studentDateOfCouurse(studentDateOfCourse)
        object.save()
    return Response({"status": "success"})

def getAllStudent():
    object = Students()
    objectreturn = object.objects.all()
    return Response({"returnval": objectreturn})

def editStudent(request):
    studentid = request.GET['studentid']
    object = Students()
    objectreturn = object.objects.get(studentid = studentid)
    return Response({"returnval": objectreturn})

def UpdateStudent(request):
    if request.method == "POST":
        studentName = request.POST['studentName']
        studentCourse = request.POST['studentCourse']
        studentTermId = request.POST['studentTermId']
        studentRoomId = request.POST['studentRoomId']
        studentDateOfCourse = request.POST['studentDateOfCourse']
        studentid = request.POST['studentid']
        object = Students()
        object.studentname(studentName)
        object.studentcourse(studentCourse)
        object.studenttermid(studentTermId)
        object.studentRoomid(studentRoomId)
        object.studentDateOfCouurse(studentDateOfCourse)
        object.studentid(studentid)
        object.save()
    return Response({"status": "success"})

def deleteStudent(request):
    studentid = request.GET['studentid']
    object = Students()
    object.objects.filter(studentid = studentid).delete()
    return Response({"status": "success"})