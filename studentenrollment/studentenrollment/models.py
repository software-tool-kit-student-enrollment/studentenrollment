from django.db import models

class User(models.Model):
    userid = models.AutoField(primary_key=True)
    username = models.CharField(max_length=50)
    password = models.CharField(max_length=50)
    class Meta:
        db_table = 'users'

class Courses(models.Model):
    courseid = models.AutoField(primary_key=True)
    courseName = models.CharField(max_length=255)
    class Meta:
        db_table = 'courses'

class Term(models.Model):
    termid = models.AutoField(primary_key=True)
    termname = models.TextField(default=None)
    termcourseid = models.IntegerField()
    class Meta:
        db_table = 'term'

class Room(models.Model):
    roomid = models.AutoField(primary_key=True)
    roomname = models.TextField(default=None)
    roomCourseId = models.IntegerField()
    class Meta:
        db_table = 'room'

class Students(models.Model):
    studentid = models.AutoField(primary_key=True)
    studentname = models.TextField()
    studentcourse = models.IntegerField()
    studenttermid = models.IntegerField()
    studentRoomid = models.IntegerField()
    studentDateOfCouurse = models.IntegerField()
    class Meta:
        db_table = 'students'

class DateOfCourse(models.Model):
    dateId = models.AutoField(primary_key=True)
    dateConducted = models.DateField()
    dateTime = models.TimeField()
    dateCourseid = models.IntegerField()
    dateTerm = models.IntegerField()
    class Meta:
        db_table = 'dateofcourse'

